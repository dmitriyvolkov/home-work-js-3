// 1.Цикл у програмуванні потрібен для того, щоб :
// - робити правельні введення даних користувачу
// - виконувати ті чі їнші дії після конктретного кола дії

// 2.
// Цикл For - буде використовуватись в ціеї домашньої роботи, для того щоб поступово виводити чісла в консоль, доки не буде те чісло яке ввів користувачу
// Цикл while - теж використовуватись в домашній роботі, цей цикл для того щоб бути в циклі до того, як він не дасть true, після цікл закінчуется

// 3.
// Явне перетвореня типу, це коли сам програміст пише: наприклад

// const number = "2"
// const numberTwo = 3
// console.log(number + numberTwo)

// тоді буде 23, тому що: перше чісло, не є числом, а рахуется як строка
// для цього, потрібно:

// console.log(Number(number) + numberTwo)
// Не явне перетворення це:
// console.log("15" / "3")
// коли Js сам перетворює тіпі за допомогою математичних рівнянь

let userNumber = +prompt('Your number')
while (userNumber == null || isNaN(userNumber) || userNumber == '') {
  userNumber = +prompt('You need write only number')
}
if (Number.isInteger(0) && userNumber < 5) {
  userNumber = alert('Sorry, no numbers')
} else {
  for (let i = 0; i < userNumber; i++) {
    if (i % 5 == 0) {
      console.log(i)
    }
  }
}
